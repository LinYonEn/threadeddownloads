package edu.sjsu.android.threadeddownloads;

import static java.security.AccessController.getContext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private EditText eText;
    private Button Runnable, Messages, ASync, Reset;
    private ImageView img;
    private ProgressDialog progress;
    private URL imgURL= null;
    private InputStream in = null;
    private Bitmap bit = null;
    private ProgressBar progressBar = null;
    Handler handler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        eText = (EditText) findViewById(R.id.URLText);
        Runnable = (Button) findViewById(R.id.Runnable);
        Messages = (Button) findViewById(R.id.Messages);
        ASync = (Button) findViewById(R.id.ASync);
        Reset = (Button) findViewById(R.id.Reset);
        img = (ImageView) findViewById(R.id.image);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    public void runRunnable(View view) {
        progress = new ProgressDialog(MainActivity.this);
        progress.setTitle("Download");
        progress.setMessage("Downloading via Runnable");
        progress.show();
        Runnable myRunnable = new MyRunnableClass();
        Thread t1 = new Thread(myRunnable);
        t1.start();
        progress.hide();
    }

    public void runMessages(View view) {
        progressBar.setProgress(0);
        Thread backgroundThread = new Thread(new Runnable() {
            public void run() {
                try {
                    for(int i = 0; i < 20; i++) {
                        Thread.sleep(300);
                        handler.sendMessage(handler.obtainMessage());
                    }
                } catch (Throwable t) {}
            }
        });
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progressBar.incrementProgressBy(5);
                if (progressBar.getProgress() == 100) {
                    progressBar.setProgress(0);;
                }
            }
        };
    }

    public void runAsyncTask(View view) throws IOException {
        String temp = eText.getText().toString();
        if(temp != null) {
            Download dl = new Download();
            dl.execute(temp);
        } else Toast.makeText(this, "Please enter a URL", Toast.LENGTH_LONG).show();
    }


    public void resetImage(View view) {
        img.setImageResource(R.drawable.apple);
    }

    private class Download extends AsyncTask<String, String, Bitmap> {
        private Context context;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(MainActivity.this);
            progress.setTitle("Download");
            progress.setMessage("Downloading via Async Task");
            progress.setIndeterminate(false);
            progress.setCancelable(false);
            progress.show();
        }
        @Override
        protected Bitmap doInBackground(String... URL) {
            try {
                String temp = eText.getText().toString();
                imgURL = new URL(temp);
                HttpURLConnection cnx = (HttpURLConnection) imgURL.openConnection();
                cnx.setDoInput(true);
                cnx.connect();
                in = cnx.getInputStream();
                bit = BitmapFactory.decodeStream(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bit;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if(img != null) {
                progress.hide();
                img.setImageBitmap(bitmap);
            } else {
                Toast.makeText(context, "Download unsuccessful", Toast.LENGTH_LONG).show();
                progress.show();
            }
        }
    }

    private class MyRunnableClass implements Runnable {
        private Context context;
        @Override
        public void run() {
            String temp = eText.getText().toString();
            try {
                in = (InputStream) new URL(temp).getContent();
                bit = BitmapFactory.decodeStream(in);
                in.close();
                img.setImageBitmap(bit);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}